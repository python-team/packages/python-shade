Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: shade
Source: http://git.openstack.org/cgit/openstack-infra/shade

Files: *
Copyright:
 2013-2015 Hewlett-Packard Development Company, L.P.
 2010-2013 OpenStack Foundation
License: Apache-2
Comment: Copyright deduced from setup.py

Files: releasenotes/*
Copyright: 2017 Shade Developers
License: Apache-2
Comment: Copyright defined in releasenotes/source/conf.py

Files: setup.py
Copyright: 2013 Hewlett-Packard Development Company, L.P.
License: Apache-2

Files: shade/_log.py
Copyright: 2015 IBM Corp.
License: Apache-2

Files: shade/_normalize.py
Copyright:
 2015 Hewlett-Packard Development Company, L.P.
 2016 Red Hat, Inc.
License: Apache-2

Files:
 shade/_heat/template_utils.py
 shade/_heat/utils.py
Copyright: 2012 OpenStack Foundation
License: Apache-2

Files:
 shade/tests/functional/test_project.py
 shade/tests/functional/test_range_search.py
Copyright: 2016 IBM
License: Apache-2

Files:
 shade/tests/base.py
 shade/tests/unit/base.py
Copyright:
 2010-2011 OpenStack Foundation
 2013 Hewlett-Packard Development Company, L.P.
License: Apache-2

Files: shade/task_manager.py
Copyright: 2011-2013 OpenStack Foundation
License: Apache-2

Files: shade/_heat/event_utils.py
Copyright: 2015 Red Hat Inc.
License: Apache-2

Files:
 shade/_adapter.py
 shade/tests/functional/test_devstack.py
Copyright: 2016 Red Hat, Inc.
License: Apache-2

Files:
 shade/tests/unit/test_availability_zones.py
 extras/install-tips.sh
Copyright: 2017 Red Hat, Inc.
License: Apache-2

Files:
 shade/tests/unit/test_qos_bandwidth_limit_rule.py
 shade/tests/unit/test_qos_dscp_marking_rule.py
 shade/tests/unit/test_qos_minimum_bandwidth_rule.py
 shade/tests/unit/test_qos_policy.py
 shade/tests/unit/test_qos_rule_type.py
 shade/tests/unit/test_router.py
 shade/tests/unit/test_subnet.py
 shade/tests/functional/test_qos_bandwidth_limit_rule.py
 shade/tests/functional/test_qos_dscp_marking_rule.py
 shade/tests/functional/test_qos_minimum_bandwidth_rule.py
 shade/tests/functional/test_qos_policy.py
Copyright: 2017 OVH SAS
License: Apache-2

Files:
 shade/meta.py
 shade/__init__.py
 doc/source/conf.py
Copyright: 2014 Hewlett-Packard Development Company, L.P.
License: Apache-2

Files:
 shade/exc.py
 shade/_utils.py
 shade/cmd/inventory.py
 shade/tests/unit/test_domains.py
 shade/tests/unit/test_endpoints.py
 shade/tests/unit/test_floating_ip_common.py
 shade/tests/unit/test_floating_ip_neutron.py
 shade/tests/unit/test_floating_ip_pool.py
 shade/tests/unit/test_floating_ip_nova.py
 shade/tests/unit/test_meta.py
 shade/tests/unit/test_operator_noauth.py
 shade/tests/unit/test_port.py
 shade/tests/unit/test_services.py
 shade/tests/unit/test_task_manager.py
 shade/tests/functional/test_endpoints.py
 shade/tests/functional/test_floating_ip.py
 shade/tests/functional/test_floating_ip_pool.py
 shade/tests/functional/test_flavor.py
 shade/tests/functional/test_inventory.py
 shade/tests/functional/test_services.py
Copyright: 2015 Hewlett-Packard Development Company, L.P.
License: Apache-2

Files:
 shade/tests/unit/test_image.py
 shade/tests/unit/test_image_snapshot.py
Copyright: 2016 Hewlett-Packard Development Company, L.P.
License: Apache-2

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 A copy of this license can be found on Debian systems in
 /usr/share/common-licenses/Apache-2.0
